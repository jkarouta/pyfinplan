from finpy.components import FixedCost, RecurringCost, AssetDepreciation
import finpy
import numpy as np
import pandas as pd
import plotly.express as px


class Scenario(object):
    def __init__(self, name, description=None):
        self.name = name
        self.description = description
        self.fixed_costs = []
        self.recurring_costs = []
        self.assets = []
        self.sim_start = 0
        self.sim_end = 36
        self.sim_res = 1

    @property
    def components(self):
        return self.fixed_costs + self.recurring_costs + self.assets

    @property
    def costs(self):
        return self.fixed_costs + self.recurring_costs

    def add_fixed_cost(self, cost: FixedCost):
        self.fixed_costs.append(cost)

    def add_recurring_cost(self, cost: RecurringCost):
        self.recurring_costs.append(cost)

    def add_asset(self, asset: AssetDepreciation):
        self.assets.append(asset)

    def cost_evolution(self, start=None, end=None, resolution=None, total=True):
        start = self.sim_start if start is None else start
        end = self.sim_end if end is None else end
        resolution = self.sim_res if resolution is None else resolution

        cost_evolution = np.array([cost.cost_evolution(start, end, resolution)
                                   for cost in self.fixed_costs + self.recurring_costs + self.assets])
        if total:
            return cost_evolution.sum(axis=0)
        else:
            return cost_evolution

    def _check_scenario(self, other):
        if not type(other) == type(self):
            raise TypeError(f'Can only subtract Scenario from other Scenario. Not {type(other).__name__}.')

    def __sub__(self, other):
        self._check_scenario(other)
        return self.cost_evolution() - other.cost_evolution(self.sim_start, self.sim_end, self.sim_res)

    def __mul__(self, other):
        """
        Scenario * other returns intersection points as list of tuples (p, s) where p is the index of the point,
            and s is a boolean which is True when Scenario is smaller than other.
        :param other:
        :return:
        """
        self._check_scenario(other)
        smaller = (self - other) <= 0
        sign_change = smaller[:-1] != smaller[1:]
        indices = list(np.where(sign_change)[0] + 1)
        indices.insert(0, 0)
        return [(i, smaller[i]) for i in indices]

    def __iter__(self):
        return iter(self.components)


def plot_compare(scenarios, start=None, end=None, resolution=None, title=None):
    sim_start = min([scenario.sim_start for scenario in scenarios]) if start is None else start
    sim_end = max([scenario.sim_end for scenario in scenarios]) if end is None else end
    sim_res = min([scenario.sim_res for scenario in scenarios]) if resolution is None else resolution

    title = 'Scenario Comparison in Costs' if title is None else title

    df = pd.DataFrame()
    for scenario in scenarios:
        scenario.sim_start = sim_start
        scenario.sim_end = sim_end
        scenario.sim_res = sim_res
        df_ = pd.DataFrame(scenario.cost_evolution())
        df_.columns = [f'cost']
        df_.set_index(pd.Index(range(sim_start, sim_end + 1, sim_res)))
        df_['Scenario'] = scenario.name
        df = pd.concat([df, df_], axis=0)

    fig = px.line(df, y='cost', color='Scenario',
                  title=title,
                  labels={
                      'cost': f'Net Cost ({finpy.CURRENCY_UNIT})',
                      'index': f'Time [{finpy.TIME_UNIT}]'
                  })

    fig.update_layout(
        hovermode='x',
        xaxis=dict(
            tickmode='linear',
            tick0=0,
            dtick=12
        )
    )

    fig.show()


def plot_breakdown(scenario, start=None, end=None, resolution=None, title=None):
    sim_start = scenario.sim_start if start is None else start
    sim_end = scenario.sim_end if end is None else end
    sim_res = scenario.sim_res if resolution is None else resolution

    title = f'Scenario {scenario.name} Cost Breakdown' if title is None else title

    df = pd.DataFrame()
    for component in scenario:
        component.sim_start = sim_start
        component.sim_end = sim_end
        component.sim_res = sim_res
        df_ = pd.DataFrame(component.cost_evolution())
        df_.columns = [f'cost']
        df_.set_index(pd.Index(range(sim_start, sim_end + 1, sim_res)))
        df_['Component'] = component.name
        df_['ComponentType'] = type(component).__name__
        df = pd.concat([df, df_], axis=0)

    fig = px.line(df, y='cost', line_dash='ComponentType', color='Component',
                  title=title,
                  labels={
                      'cost': f'Net Cost ({finpy.CURRENCY_UNIT})',
                      'index': f'Time [{finpy.TIME_UNIT}]'
                  })

    fig.update_layout(
        hovermode='x',
        xaxis=dict(
            tickmode='linear',
            tick0=0,
            dtick=12
        )
    )

    fig.show()
