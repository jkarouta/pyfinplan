from finpy.commons.constants import *
import numpy as np


class FixedCost(object):
    def __init__(self, name, unit_cost, description=None, quantity=1, start_date=0):
        self.name = name
        self.description = description
        self.name = name
        self.unit_cost = unit_cost
        self.quantity = quantity
        self.start_date = start_date
        self.sim_start = start_date
        self.sim_end = 12
        self.sim_res = 1

    @property
    def cost(self):
        return self.unit_cost * self.quantity

    def cost_on(self, date):
        if date < self.start_date:
            return 0
        else:
            return round(self.cost, 2)

    def cost_evolution(self, start=None, end=None, resolution=None):
        start = self.sim_start if start is None else start
        end = self.sim_end if end is None else end
        resolution = self.sim_res if resolution is None else resolution
        return [self.cost_on(date) for date in range(start, end + 1, resolution)]

    def __str__(self):
        string = f'{type(self).__name__}\n' \
                 f'{self.name} - {self.cost}{CURRENCY_UNIT}'
        return string


class RecurringCost(FixedCost):
    def __init__(self, name, unit_cost, runtime,
                 description=None,
                 installment_period=None, n_installments=None, quantity=1, start_date=0):
        super().__init__(name, unit_cost, description, quantity, start_date)

        if installment_period is None and n_installments is None:
            raise ValueError('Either installment_period or n_installments needs to be inputted for a Recurring Cost.')

        self.runtime = runtime
        if installment_period is not None:
            self.installment_period = installment_period
            self.n_installments = self.runtime // self.installment_period
        if n_installments is not None:
            self.n_installments = n_installments
            self.installment_period = self.runtime / self.n_installments

    @property
    def end_date(self):
        return self.start_date + self.runtime

    def cost_on(self, date):
        if date < self.start_date:
            return 0
        elif date >= self.end_date:
            return round(self.n_installments * self.cost, 2)
        else:
            time_delta = date - self.start_date
            n_transactions = np.floor(time_delta / self.installment_period) + 1
            # n_transactions = time_delta // self.installment_period
            # https://stackoverflow.com/questions/38588815/rounding-errors-in-python-floor-division
            return round(n_transactions * self.cost, 2)

    def __str__(self):
        string = super().__str__() + '\n' \
                 f'n_inst = {self.n_installments}\n' \
                 f'inst period = {self.installment_period} [{TIME_UNIT}]'
        return string


class AssetDepreciation(FixedCost):
    ALLOWED_INTERPOLATION = ['discrete', None, 'linear']
    ALLOWED_OVER = ['original value', 'current value']

    def __init__(self, name, original_value, change_percent, change_period,
                 description=None, interpolation='linear', change_over='original value',
                 quantity=1, start_date=0):
        super().__init__(name, None, description, quantity, start_date)
        if interpolation not in self.ALLOWED_INTERPOLATION:
            raise ValueError(f'Interpolation method unknown, choose from {self.ALLOWED_INTERPOLATION}.')
        self.interpolation = interpolation
        if change_over not in self.ALLOWED_OVER:
            raise ValueError(f'Interpolation method unknown, choose from {self.ALLOWED_OVER}.')
        if change_over == 'current value':
            raise NotImplementedError('change_over = "current value" not yet implemented. '
                                      'Use "original value" instead.')
        self.change_over = change_over
        self.original_value = original_value
        if 0 <= change_percent <= 1:
            self.change_percent = change_percent
        elif 0 <= change_percent <= 100:
            self.change_percent = change_percent / 100
        else:
            raise ValueError('depreciation_percent must be between 0 and 100 (%) or a fraction between 0 an 1')
        self.change_period = change_period
        self.unit_cost = original_value * self.change_percent

    def cost_on(self, date):
        if date < self.start_date:
            return 0
        else:
            time_delta = date - self.start_date
            if self.interpolation == 'linear':
                n_transactions = time_delta / self.change_period
            else:  # self.method == 'discrete'
                n_transactions = np.floor(time_delta / self.change_period)
            return min(-self.original_value + round(n_transactions * self.cost, 2), 0)

    def value_on(self, date):
        return -self.cost_on(date)


class AssetAppreciation(AssetDepreciation):
    def __init__(self, name, original_value, change_percent, change_period,
                 description=None, interpolation='linear', change_over='original value',
                 quantity=1, start_date=0):
        super().__init__(name, original_value, -change_percent, change_period,
                         description, interpolation, change_over,
                         quantity, start_date)
