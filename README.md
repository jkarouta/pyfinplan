# PyFinPlan

## Description
PyFinPlan is a Python libary containing utilities for financial planning. Comparing different buying and payment strategies, including loans, mortgages and asset depreciation and appreciation. 

## Installation
1. Clone the repository to /path/
2. Install using pip:
```commandline
pip install /path/.
```
## Usage
Check out the example folder for usage examples.
A small example will be added here later.

## Support
Encountered an issue or bug? Please log them on the [issue page](https://gitlab.com/jkarouta/pyfinplan/-/issues)
Any other questions please email jeremy (at) karouta (dot) net

## Roadmap
- [ ] Add Mortages and Loans
- [ ] Add small example to README as well

## Contributing
Currently I'm developping this library in my spare time. If you wish to contribute, feel free to send in a merge request, or send me a message on jeremy (at) karouta (dot) net

## License
This projects is licenced under the MIT License. For more information please refer to the LICENCE file.
