import finpy
from finpy.scenarios import Scenario
from finpy.components import FixedCost, RecurringCost

finpy.CURRENCY_UNIT = '$'

sim_time = 10*12  # 10 years

scenario1 = Scenario('New Fiat')
scenario1.add_fixed_cost(FixedCost(name='Fiat 500 2022', unit_cost=17000))
scenario1.add_recurring_cost(RecurringCost(name='Insurance', runtime=sim_time, unit_cost=50, installment_period=1))

finpy.scenarios.plot_breakdown(scenario1, start=0, end=36, resolution=1)


scenario2 = Scenario('Old Fiat')
scenario2.add_fixed_cost(FixedCost(name='Fiat 500 2012', unit_cost=12000))
scenario2.add_recurring_cost(RecurringCost(name='Insurance', runtime=sim_time, unit_cost=50, installment_period=1))
scenario2.add_recurring_cost(RecurringCost(name='Maintenance', runtime=sim_time, unit_cost=1500, installment_period=12))

finpy.scenarios.plot_compare([scenario1, scenario2], start=0, end=36, resolution=1)
