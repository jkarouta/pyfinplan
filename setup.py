from setuptools import setup
import re

with open('pylime/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

setup(
    name='finpy',
    version=version,
    packages=['finpy'],
    url='https://gitlab.com/jkarouta/pyfinplan',
    license='MIT',
    author='jkarouta',
    author_email='jeremy@karouta.net',
    description='Financial Planning Library'
)
